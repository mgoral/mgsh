/**
 * Copyright (C) 2016 Michał Góral.
 * 
 * This file is part of mgsh
 * 
 * mgsh is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * mgsh is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with mgsh. If not, see <http://www.gnu.org/licenses/>.
 */

#include <string>
#include <algorithm>
#include <unordered_map>

#include "parser.hpp"

namespace {

const std::unordered_map<std::string, command_t> token_map = {
    {"cd", &cmd_cd},
    {"exit", &cmd_exit},
};

command_t find_func(const std::string &name)
{
    auto found = token_map.find(name);
    if (found == token_map.end())
        return {};
    return found->second;
}

std::vector<std::string> tokenize(const std::string &in,
                                  const std::string &delim = " \n\r")
{
    std::vector<std::string> ret;

    auto from = in.find_first_not_of(delim);
    auto to = in.find_first_of(delim, from);
    while (std::string::npos != from || std::string::npos != to) {
        ret.push_back(in.substr(from, to - from));
        from = in.find_first_not_of(delim, to);
        to = in.find_first_of(delim, from);
    }
    return ret;
}

}  // namespace


callable_t parse(const std::string &line)
{
    auto tokens = tokenize(line);
    if (tokens.empty())
        return {};

    auto found = find_func(tokens[0]);
    if (!found)
        return {&cmd_exec, tokens};
    else
        return {found, {tokens.begin()+1, tokens.end()}};
}
