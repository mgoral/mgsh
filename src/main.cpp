/**
 * Copyright (C) 2016 Michał Góral.
 * 
 * This file is part of mgsh
 * 
 * mgsh is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * mgsh is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with mgsh. If not, see <http://www.gnu.org/licenses/>.
 */

#include <string>
#include <iostream>

#include <unistd.h>

#include "commands.hpp"
#include "parser.hpp"
#include "error.hpp"

const char *ps()
{
    return "> ";
}

std::string readline()
{
    std::string line;
    getline(std::cin, line);
    return line;
}

void run()
{
    while (true) {
        printf("%s %s", getcwd(NULL, 0), ps());
        const auto &line = readline();

        auto call = parse(line);
        if (!call.cmd)
            continue;

        if (call.cmd(call.args))
        {
            last_error();
        }
    }
}

int main(int argc, char **argv)
{
    run();
    return 0;
}
