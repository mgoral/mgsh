/**
 * Copyright (C) 2016 Michał Góral.
 * 
 * This file is part of mgsh
 * 
 * mgsh is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * mgsh is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with mgsh. If not, see <http://www.gnu.org/licenses/>.
 */

#include <cstdlib>

#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>

#include "commands.hpp"
#include "error.hpp"

namespace {

const char *home()
{
    return getenv("HOME");
}

std::vector<char *> to_char(const std::vector<std::string> &in)
{
    std::vector<char *> ret;
    for (const auto &s : in)
        ret.emplace_back(const_cast<char *>(s.c_str()));
    return ret;
}

}  // namespace

int cmd_cd(const std::vector<std::string> &args)
{
    int result = args.empty() ? chdir(home()) : chdir(args[0].c_str());
    return result;
}

int cmd_exit(const std::vector<std::string> &args)
{
    std::exit(EXIT_SUCCESS);
}

int cmd_exec(const std::vector<std::string> &args)
{
    pid_t pid;

    pid = fork();
    if (pid == 0) {
        auto argc = to_char(args);
        argc.push_back(nullptr);
        if(-1 == execvp(argc[0], argc.data()))
            last_error();
    }
    else if (pid < 0) {
        last_error();
    }
    else {
        int status;
        do {
            waitpid(pid, &status, WUNTRACED);
        } while (!WIFEXITED(status) && !WIFSIGNALED(status));
    }
    return 0;
}
