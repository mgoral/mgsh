/**
 * Copyright (C) 2016 Michał Góral.
 * 
 * This file is part of mgsh
 * 
 * mgsh is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * mgsh is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with mgsh. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COMMANDS_HPP_
#define COMMANDS_HPP_

#include <string>
#include <vector>
#include <functional>

using command_t = std::function<int(const std::vector<std::string> &)>;

/// INTERNAL COMMANDS

int cmd_cd(const std::vector<std::string> &args);
int cmd_exit(const std::vector<std::string> &args);

/// EXTERNAL COMMANDS

int cmd_exec(const std::vector<std::string> &args);

#endif
